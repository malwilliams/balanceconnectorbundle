<?php

namespace Balance\Bundle\BalanceConnectorBundle;

use Akeneo\Bundle\BatchBundle\Connector\Connector;

/**
 * Balance connector to import/export data from test platform
 *
 */
class BalanceBalanceConnectorBundle extends Connector
{
}